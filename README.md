# TIEscola

Sistema para gerenciar equipamentos eletrônicos de uma escola.

## Configurando

Primeiramente é necessário que tenhamos o [Python 3.7+](https://www.python.org/) instalado em nossa máquina.

Depois devemos instalar o ambiente virtual:

```
pip install virtualenv
```

Então podemos criar e inicializar o ambiente virtual:

```
python3 -m venv env 
source env/bin/activate
```

Instalamos as bibliotecas necessárias para o sistema operar:

```
pip install -r requirements.txt
```

Por fim, executamos ele:

```
python3 manage.py runserver
```

Podemos agora acessá-lo no endereço: `http://127.0.0.1:8000/`

Para efetuar os testes automatizados:

```
python3 manage.py test
```