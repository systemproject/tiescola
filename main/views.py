from .forms import  (
    ManutencaoForm,
    ChamadaForm,
    AtivoTIForm,
    ChamadaForm2,
    ComentarioForm,
    ManutencaoAtivoForm,
    ChamadaAtivoForm,
    ChamadasAtivoForm,
    ChamadaAtivoFormSemChamada,
    ChamadaAtivoFormChamada
)
from .models import Chamada, AtivoTI, Comentario, ManutencaoAtivo, ChamadaAtivo
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.http import HttpResponse, JsonResponse
from django.db.models.functions import TruncDate
from django.core.serializers import serialize
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib import messages
from reportlab.pdfgen import canvas
from django.db.models import Count
from django.db import transaction
from django.urls import reverse
from django.views import View
from decimal import Decimal
from io import BytesIO
from PIL import Image
import qrcode
import base64
import math
import json
import csv

@login_required
def index(request):
    return render(request, 'main/index.html')

@login_required
def business_intelligence(request):
    return render(request, 'main/business_intelligence.html')

@login_required
def data_export(request):
    return render(request, 'main/exportar_dados.html')

@login_required
def data_import(request):
    return render(request, 'main/importar_dados.html')

@login_required
def fazer_logout(request):
    logout(request)
    return redirect('login')

@login_required
def ativo_detail_view(request, id):
    ativo = get_object_or_404(AtivoTI, id=id)
    total_chamadas = len([chamada for chamada in ativo.chamada_set.all()])
    manutencoes = [manutencao for manutencao in ativo.manutencaoativo_set.all()]

    chamadas = ativo.chamada_set.all().order_by('data_abertura')
    paginator = Paginator(chamadas, 5)
    page_number = request.GET.get('page')

    try:
        chamadas_paginadas = paginator.page(page_number)
    except PageNotAnInteger:
        chamadas_paginadas = paginator.page(1)
    except EmptyPage:
        chamadas_paginadas = paginator.page(paginator.num_pages)

    chamadas_associadas = ativo.chamadaativo_set.all().order_by('-chamada')
    total_chamadas_associadas = len(chamadas_associadas)
    paginator_associados = Paginator(chamadas_associadas, 5)
    page_number = request.GET.get('page_associados')

    try:
        chamadas_associadas_paginadas = paginator_associados.page(page_number)
    except PageNotAnInteger:
        chamadas_associadas_paginadas = paginator_associados.page(1)
    except EmptyPage:
        chamadas_associadas_paginadas = paginator_associados.page(paginator_associados.num_pages)

    if request.method == 'POST' and 'descricao' in request.POST:
        form = ManutencaoForm(request.POST)
        if form.is_valid():
            manutencao = form.save(commit=False)
            manutencao.ativo = ativo
            manutencao.save()
            messages.success(request, 'Manutenção Cadastrada com Sucesso')
            return redirect('ativo_detail', id=id)
    else:
        form = ManutencaoForm()

    if request.method == 'POST' and 'descricao_problema' in request.POST:
        chamada_form = ChamadaForm(request.POST)
        if chamada_form.is_valid():
            chamada = chamada_form.save(commit=False)
            chamada.ativo_ti = ativo
            chamada.save()
            messages.success(request, 'Chamado Aberto com Sucesso')
            return redirect('ativo_detail', id=id)
    else:
        initial_data = {'ativo_ti': ativo.id} 
        chamada_form = ChamadaForm(initial=initial_data)

    url_ativo = request.build_absolute_uri(reverse('ativo_detail', args=[ativo.id]))

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=5,
        border=4,
    )
    qr.add_data(url_ativo)
    qr.make(fit=True)

    img = qr.make_image(fill_color="black", back_color="white")

    buffer = BytesIO()
    img.save(buffer)
    buffer.seek(0)

    image_file = InMemoryUploadedFile(
        buffer,
        None,
        'qrcode.png',
        'image/png',
        buffer.getbuffer().nbytes,
        None
    )

    encoded_image = base64.b64encode(buffer.getvalue()).decode('utf-8')

    return render(request, 
        'main/detalhes_ativo.html', 
        {
            'ativo': ativo,
            'chamadas': chamadas_paginadas,
            'total_chamadas': total_chamadas,
            'manutencoes': manutencoes,
            'qr_code': encoded_image,
            'form': form,
            'chamada_form': chamada_form,
            'chamadas_associadas': chamadas_associadas_paginadas,
            'total_chamadas_associadas': total_chamadas_associadas
        }
    )

@login_required
def editar_ativo(request, codigo):
    # Obtém o objeto AtivoTI que você deseja editar ou retorna um erro 404 se não existir
    ativo = get_object_or_404(AtivoTI, codigo=codigo)

    if request.method == 'POST':
        # Preenche o formulário com os dados enviados no POST e os dados existentes do objeto AtivoTI
        form = AtivoTIForm(request.POST, instance=ativo)
        if form.is_valid():
            form.save()
            messages.success(request, f'{ativo.nome} Alterado com Sucesso')
            return redirect('ativo_detail', id=ativo.id)
    else:
        # Cria um formulário preenchido com os dados existentes do objeto AtivoTI
        form = AtivoTIForm(instance=ativo)

    # Renderiza o template com o formulário
    return render(request, 'main/editar_ativo.html', {'form': form, 'ativo': ativo})

@login_required
def remover_ativo(request, codigo):
    ativo = get_object_or_404(AtivoTI, codigo=codigo)
    messages.success(request, f'{ativo.nome} Removido com Sucesso')
    ativo.delete()
    return redirect('listar_ativos')

@login_required
def remover_chamada(request, id):
    chamada = get_object_or_404(Chamada, id=id)
    messages.success(request, f'{chamada.id} Removida com Sucesso')
    chamada.delete()
    return redirect('listar_chamadas')

@login_required
def remover_chamada_lista(request, id, codigo):
    chamada = get_object_or_404(Chamada, id=id)
    messages.success(request, f'{chamada.id} Removida com Sucesso')
    chamada.delete()
    return redirect('ativo_detail', id=chamada.ativo_ti.id)

@login_required
def remover_comentario(request, id, chamada_id):
    comentario = get_object_or_404(Comentario, id=id)
    messages.success(request, f'Comentário {comentario.id} Removido com Sucesso')
    comentario.delete()
    return redirect('chamada_detail', id=chamada_id)

@login_required
def editar_chamada(request, id):
    # Obtém o objeto AtivoTI que você deseja editar ou retorna um erro 404 se não existir
    chamada = get_object_or_404(Chamada, id=id)

    if request.method == 'POST':
        # Preenche o formulário com os dados enviados no POST e os dados existentes do objeto AtivoTI
        form = ChamadaForm2(request.POST, instance=chamada)
        if form.is_valid():
            form.save()
            messages.success(request, f'{chamada.id} Alterada com Sucesso')
            return redirect('chamada_detail', id=id)
    else:
        # Cria um formulário preenchido com os dados existentes do objeto AtivoTI
        form = ChamadaForm2(instance=chamada)

    # Renderiza o template com o formulário
    return render(request, 'main/editar_chamada.html', {'form': form, 'chamada': chamada})

@login_required
def editar_manutencao(request, id, codigo):
    # Obtém o objeto AtivoTI que você deseja editar ou retorna um erro 404 se não existir
    manutencao = get_object_or_404(ManutencaoAtivo, id=id)

    if request.method == 'POST':
        # Preenche o formulário com os dados enviados no POST e os dados existentes do objeto AtivoTI
        form = ManutencaoForm(request.POST, instance=manutencao)
        if form.is_valid():
            form.save()
            messages.success(request, f'{manutencao.id} Alterada com Sucesso')
            return redirect('ativo_detail', id=manutencao.ativo.id)
    else:
        # Cria um formulário preenchido com os dados existentes do objeto AtivoTI
        form = ManutencaoForm(instance=manutencao)

    # Renderiza o template com o formulário
    return render(request, 'main/editar_manutencao.html', {'form': form, 'manutencao': manutencao, 'codigo': codigo})

@login_required
def cadastrar_manutencao_ativo(request):
    if request.method == 'POST':
        form = ManutencaoAtivoForm(request.POST)
        if form.is_valid():
            ativos_selecionados = form.cleaned_data['ativos']
            descricao = form.cleaned_data['descricao']
            data_inicio = form.cleaned_data['data_inicio']
            data_conclusao = form.cleaned_data['data_conclusao']
            custo = form.cleaned_data['custo']
            responsavel = form.cleaned_data['responsavel']
            
            for ativo in ativos_selecionados:
                ManutencaoAtivo.objects.create(
                    ativo=ativo,
                    descricao=descricao,
                    data_inicio=data_inicio,
                    data_conclusao=data_conclusao,
                    custo=custo,
                    responsavel=responsavel
                )
            
            messages.success(request, 'Manutenções Registradas com Sucesso')
            return redirect('listar_ativos')
    else:
        form = ManutencaoAtivoForm()
    return render(request, 'main/cadastrar_manutencoes.html', {'form': form})

@login_required
def editar_comentario(request, id, chamada_id):
    # Obtém o objeto AtivoTI que você deseja editar ou retorna um erro 404 se não existir
    comentario = get_object_or_404(Comentario, id=id)

    if request.method == 'POST':
        # Preenche o formulário com os dados enviados no POST e os dados existentes do objeto AtivoTI
        form = ComentarioForm(request.POST, instance=comentario)
        if form.is_valid():
            form.save()
            messages.success(request, f'{comentario.id} Alterada com Sucesso')
            return redirect('chamada_detail', id=chamada_id)
    else:
        # Cria um formulário preenchido com os dados existentes do objeto AtivoTI
        form = ComentarioForm(instance=comentario)

    # Renderiza o template com o formulário
    return render(request, 'main/editar_comentario.html', {'form': form, 'comentario': comentario, 'chamada_id': chamada_id})

@login_required
def remover_manutencao(request, id, codigo):
    manutencao = get_object_or_404(ManutencaoAtivo, id=id)
    messages.success(request, f'Manutenção {manutencao.id} Removida com Sucesso')
    manutencao.delete()
    return redirect('ativo_detail', id=manutencao.ativo.id)

@login_required
def chamada_detail_view(request, id):
    chamada = get_object_or_404(Chamada, id=id)
    total_comentarios = len([comentario for comentario in chamada.comentario_set.all()])

    ativos_associados = []
    for chamada_ativo in ChamadaAtivo.objects.filter(chamada=id):
        ativos_associados.extend(chamada_ativo.ativos.all())

    total_ativos_associados = len(ativos_associados)

    # Paginação dos ativos associados
    paginator_associados = Paginator(ativos_associados, 5)  # Defina o número de ativos por página
    page_number = request.GET.get('page_ativos')

    try:
        ativos_paginados = paginator_associados.page(page_number)
    except PageNotAnInteger:
        ativos_paginados = paginator_associados.page(1)
    except EmptyPage:
        ativos_paginados = paginator_associados.page(paginator_associados.num_pages)

    comentarios = chamada.comentario_set.all().order_by('data_publicacao')
    paginator = Paginator(comentarios, 5)
    page_number = request.GET.get('page')

    try:
        comentarios_paginados = paginator.page(page_number)
    except PageNotAnInteger:
        comentarios_paginados = paginator.page(1)
    except EmptyPage:
        comentarios_paginados = paginator.page(paginator.num_pages)

    return render(request, 
        'main/detalhes_chamada.html', 
        {
            'chamada': chamada,
            'comentarios': comentarios_paginados,
            'total_comentarios': total_comentarios,
            'ativos_associados': ativos_paginados,
            'total_ativos_associados': total_ativos_associados
        }
    )

@login_required
@transaction.atomic
def associar_ativos_chamada(request):
    if request.method == 'POST':
        form = ChamadaAtivoForm(request.POST)
        if form.is_valid():
            chamada_selecionada = form.cleaned_data['chamada']
            ativos_selecionados = form.cleaned_data['ativo']

            for ativo in ativos_selecionados:
                chamada_ativo = ChamadaAtivo.objects.create(chamada=chamada_selecionada)
                chamada_ativo.ativos.add(ativo)

            messages.success(request, 'Associação Realizada com Sucesso')
            return redirect('chamada_detail', id=chamada_selecionada.id)
    else:
        form = ChamadaAtivoForm()

    return render(request, 'main/associar_ativos_chamada.html', {'form': form})

@login_required
@transaction.atomic
def associar_ativos_chamada_especifica(request, chamada_id):
    if request.method == 'POST':
        form = ChamadaAtivoFormSemChamada(chamada_id=chamada_id, data=request.POST)
        if form.is_valid():
            chamada_selecionada = Chamada.objects.get(id=chamada_id)
            ativos_selecionados = form.cleaned_data['ativo']

            for ativo in ativos_selecionados:
                chamada_ativo = ChamadaAtivo.objects.create(chamada=chamada_selecionada)
                chamada_ativo.ativos.add(ativo)

            messages.success(request, 'Associação Realizada com Sucesso')
            return redirect('chamada_detail', id=chamada_id)
    else:
        form = ChamadaAtivoFormSemChamada(chamada_id=chamada_id)

    return render(request, 'main/associar_ativos_chamada_especifica.html', {'form': form, 'chamada_id': chamada_id})

@login_required
@transaction.atomic
def associar_chamadas_ativo(request, ativo_id):
    if request.method == 'POST':
        form = ChamadaAtivoFormChamada(ativo_id=ativo_id, data=request.POST)
        if form.is_valid():
            ativo_selecionado = AtivoTI.objects.filter(id=ativo_id).first()
            chamadas_selecionadas = form.cleaned_data['chamadas']

            for chamada in chamadas_selecionadas:
                chamada_ativo = ChamadaAtivo.objects.create(chamada=chamada)
                chamada_ativo.ativos.add(ativo_selecionado)

            messages.success(request, 'Associação Realizada com Sucesso')
            return redirect('ativo_detail', id=ativo_id)
    else:
        form = ChamadaAtivoFormChamada(ativo_id=ativo_id)

    return render(request, 'main/associar_chamadas_ativo.html', {'form': form, 'ativo_id': ativo_id})

@login_required
@transaction.atomic
def associar_chamadas_geral_ativo(request):
    if request.method == 'POST':
        form = ChamadasAtivoForm(request.POST)
        if form.is_valid():
            ativo_selecionado = form.cleaned_data['ativos']
            chamadas_selecionadas = form.cleaned_data['chamadas']

            for chamada in chamadas_selecionadas:
                chamada_ativo = ChamadaAtivo.objects.create(chamada=chamada)
                chamada_ativo.ativos.add(ativo_selecionado)

            messages.success(request, 'Associação Realizada com Sucesso')
            return redirect('listar_ativos')
    else:
        form = ChamadasAtivoForm()

    return render(request, 'main/associar_chamadas_geral_ativo.html', {'form': form})

def get_ativos_disponiveis(request):
    chamada_id = request.GET.get('chamada_id')
    if not chamada_id:
        return JsonResponse({})
    ativos_disponiveis = list(AtivoTI.objects.exclude(chamadaativo__chamada_id=chamada_id).values('id', 'nome'))
    ativo_ti_id = Chamada.objects.filter(id=chamada_id).values('ativo_ti_id').first()["ativo_ti_id"]
    ativos_resultado = [ativo for ativo in ativos_disponiveis if ativo['id'] != ativo_ti_id]
    return JsonResponse({'ativos': ativos_resultado})

def get_chamadas_disponiveis(request):
    ativo_id = request.GET.get('ativo_id')
    if not ativo_id:
        return JsonResponse({})

    # Verifica se há uma Chamada relacionada ao Ativo
    chamada_id_result = Chamada.objects.filter(ativo_ti_id=ativo_id).values('id').first()
    if chamada_id_result is not None:
        chamada_id = chamada_id_result['id']
    else:
        chamada_id = None

    # Filtra as chamadas disponíveis, excluindo aquela associada ao ativo
    chamadas_disponiveis = Chamada.objects.exclude(chamadaativo__ativos__id=ativo_id).values('id', 'status')
    chamadas_resultado = [chamada for chamada in chamadas_disponiveis if chamada['id'] != chamada_id]

    return JsonResponse({'chamadas': chamadas_resultado})

@login_required
def desassociar_ativo_chamada(request, chamada_id, ativo_id):
    # Obtém a chamada e o ativo
    chamada = get_object_or_404(Chamada, id=chamada_id)
    ativo = get_object_or_404(AtivoTI, id=ativo_id)

    # Verifica se o ativo está associado à chamada
    try:
        chamada_ativo = ChamadaAtivo.objects.filter(chamada=chamada, ativos__id=ativo_id).first()
    except ChamadaAtivo.DoesNotExist:
        messages.success(request, f'O ativo não está associado a esta chamada.')
        return redirect('chamada_detail', id=chamada.id)

    if chamada_ativo:
        # Desassocia o ativo da chamada
        chamada_ativo.ativos.remove(ativo)
        messages.success(request, f'Desassociação Realizada com Sucesso')
    else:
        messages.success(request, f'O ativo não está associado a esta chamada.')

    return redirect('chamada_detail', id=chamada.id)

@login_required
def adicionar_comentario_view(request, id):
    chamada = get_object_or_404(Chamada, id=id)

    if request.method == 'POST':
        texto = request.POST.get('texto')
        if texto:
            Comentario.objects.create(chamada=chamada, autor=request.user, texto=texto)
            messages.success(request, 'Comentário Incluído no Chamado')
            # Redirecione para a mesma página após adicionar o comentário
            return redirect('chamada_detail', id=id)

    return redirect('chamada_detail', id=id)

@login_required
def situacao_chart(request):
    ativos = AtivoTI.objects.all()
    situacao_data = [ativo.get_situacao_data() for ativo in ativos]
    situacao_data = situacao_data[0] if situacao_data else []
    return render(request, 'main/chart_situacao.html', {'situacao_data': situacao_data})

@login_required
def tipo_chart(request):
    ativos = AtivoTI.objects.all()
    tipo_data = [ativo.get_tipo_data() for ativo in ativos]
    tipo_data = tipo_data[0] if tipo_data else []
    return render(request, 'main/chart_tipo.html', {'tipo_data': tipo_data})

@login_required
def marca_chart(request):
    ativos = AtivoTI.objects.all()
    marca_data = [ativo.get_marcas_data() for ativo in ativos]
    marca_data = marca_data[0] if marca_data else []
    return render(request, 'main/chart_marca.html', {'marca_data': marca_data})

@login_required
def status_chart(request):
    status = Chamada.objects.all()
    status_data = [stat.get_status_data() for stat in status]
    status_data = status_data[0] if status_data else []
    return render(request, 'main/chart_status.html', {'status_data': status_data})

@login_required
def prioridade_chart(request):
    prioridades = Chamada.objects.all()
    prioridade_data = [prioridade.get_prioridade_data() for prioridade in prioridades]
    prioridade_data = prioridade_data[0] if prioridade_data else []
    return render(request, 'main/chart_prioridade.html', {'prioridade_data': prioridade_data})

@login_required
def chamadas_por_sala_chart(request):
    salas = Chamada.objects.all()
    sala_data = [sala.get_sala_origem_data() for sala in salas]
    sala_data = sala_data[0] if sala_data else []
    return render(request, 'main/chart_chamadas_por_sala.html', {'sala_data': sala_data})

@login_required
def chamadas_por_data_chart(request):
    chamadas_por_data = Chamada.objects \
        .annotate(data_abertura_date=TruncDate('data_abertura')) \
        .values('data_abertura_date') \
        .annotate(num_chamadas=Count('id')) \
        .order_by('data_abertura_date')

    labels = [chamada['data_abertura_date'].strftime('%d-%m-%Y') for chamada in chamadas_por_data]
    valores = [chamada['num_chamadas'] for chamada in chamadas_por_data]

    return render(request, 'main/chamadas_por_data.html', {'labels': labels, 'valores': valores})

@login_required
def valores_estimados_chart(request):
    # Recupera os valores estimados de todos os ativos
    valores_estimados = [float(ativo.valor_estimado) for ativo in AtivoTI.objects.all() if ativo.valor_estimado is not None]

    # Calcula o número de bins usando a regra de Sturges
    num_bins = int(1 + 3.322 * math.log(len(valores_estimados)))

    # Calcula os limites dos bins
    valor_min = min(valores_estimados)
    valor_max = max(valores_estimados)
    bin_width = (valor_max - valor_min) / num_bins

    bins = [round(valor_min + i * bin_width,2) for i in range(num_bins + 1)]

    # Calcula a frequência de cada valor estimado em cada bin
    frequencia = [0] * num_bins
    for valor in valores_estimados:
        if valor == valor_max:
            index = num_bins - 1
        else:
            index = int((valor - valor_min) // bin_width)
        frequencia[index] += 1

    # Renderiza o template com os dados do histograma
    return render(request, 'main/chart_valores_estimados.html', {'bins': bins[:-1], 'frequencia': frequencia})

@login_required
def associacao_equipamento_chamada_chart(request):
    associacoes = ChamadaAtivo.objects.all()
    equipamento_associacoes = {}

    # Conta quantas vezes cada equipamento foi associado a uma chamada
    for associacao in associacoes:
        for equipamento in associacao.ativos.all():
            if equipamento.nome not in equipamento_associacoes:
                equipamento_associacoes[equipamento.nome] = 0
            equipamento_associacoes[equipamento.nome] += 1

    # Prepara os dados para o gráfico
    labels = list(equipamento_associacoes.keys())
    counts = list(equipamento_associacoes.values())

    # Renderiza o template com os dados do gráfico
    return render(request, 'main/chart_associacao_equipamento_chamada.html', {'labels': labels, 'counts': counts})

@login_required
def equipamentos_associados_por_chamada_chart(request):
    # Consulta para obter o número total de equipamentos associados a cada chamada
    dados_grafico = (
        ChamadaAtivo.objects.values('chamada')
        .annotate(total_equipamentos=Count('ativos'))
        .order_by('chamada')
    )

    # Prepara os dados para o gráfico
    labels = [f"Chamada {dado['chamada']}" for dado in dados_grafico]
    counts = [dado['total_equipamentos'] for dado in dados_grafico]

    # Renderiza o template com os dados do gráfico
    return render(request, 'main/chart_equipamentos_associados_por_chamada.html', {'labels': labels, 'counts': counts})

@login_required
def chamadas_por_equipamento(request):
    data = []
    ativos = AtivoTI.objects.all()

    for ativo in ativos:
        data.append({
            'nome': ativo.nome,
            'numero_chamadas': ativo.get_numero_chamadas(),
        })

    return JsonResponse(data, safe=False)

@login_required
def pagina_chamadas_por_equipamento(request):
    return render(request, 'main/chart_chamadas_por_equipamento.html')

@login_required
def manutencoes_por_equipamento(request):
    data = []
    ativos = AtivoTI.objects.all()

    for ativo in ativos:
        data.append({
            'nome': ativo.nome,
            'numero_manutencoes': ativo.get_numero_manutencoes(),
        })

    return JsonResponse(data, safe=False)

@login_required
def pagina_manutencoes_por_equipamento(request):
    return render(request, 'main/chart_manutencoes_por_equipamento.html')

@login_required
def listar_chamadas(request):
    # Recupera todas as chamadas
    chamadas = Chamada.objects.all().order_by('data_abertura')

    total_chamadas = len(chamadas)

    total_reparo = sum([(chamada.custo_reparo) for chamada in chamadas if isinstance(chamada.custo_reparo, Decimal)])

    if request.method == 'POST' and 'descricao_problema' in request.POST:
        chamada_form2 = ChamadaForm2(request.POST)
        if chamada_form2.is_valid():
            chamada = chamada_form2.save(commit=False)
            chamada.save()
            messages.success(request, 'Chamado Aberto com Sucesso')
            return redirect('listar_chamadas')
    else:
        chamada_form2 = ChamadaForm2()

    return render(request, 'main/listar_chamadas.html', 
        {
            'chamadas': chamadas, 
            'chamada_salas': Chamada.SALAS,
            'chamada_prioridades': Chamada.PRIORIDADE_CHOICES,
            'chamada_status': Chamada.STATUS_CHOICES,
            'total_chamadas': total_chamadas, 
            'total_reparo': total_reparo,
            'chamada_form2': chamada_form2
        }
    )

@login_required
def listar_ativos(request):
    if request.method == 'POST':
        form = AtivoTIForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Equipamento Registrado com Sucesso')
            return redirect('listar_ativos')
    else:
        form = AtivoTIForm()

    ativos = AtivoTI.objects.all().order_by('codigo')

    total_ativos = len(ativos)

    total_em_ativos = sum([(ativo.valor_estimado) for ativo in ativos if isinstance(ativo.valor_estimado, Decimal)])

    # Obtém valores únicos para os filtros
    ativo_tipos = AtivoTI.TIPOS_DE_ATIVOS
    ativo_marcas = AtivoTI.MARCAS
    ativo_situacoes = AtivoTI.SITUACOES

    return render(request, 'main/listar_ativos.html',
        {
            'ativos': ativos, 
            'ativo_tipos': ativo_tipos, 
            'ativo_marcas': ativo_marcas,
            'ativo_situacoes': ativo_situacoes,
            'total_ativos': total_ativos,
            'total_em_ativos': total_em_ativos,
            'form': form
        }
    )

@method_decorator(login_required, name='dispatch')
class ExportarChamadaCSVView(View):
    def get(self, request, *args, **kwargs):
        # Obtenha os dados do modelo (ajuste conforme necessário)
        chamadas = Chamada.objects.all()

        # Configurar a resposta HTTP para um arquivo CSV
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="exportacao_chamadas.csv"'

        # Cria um escritor CSV e escreve os cabeçalhos e dados
        writer = csv.writer(response)
        writer.writerow(['Sala Origem', 'Descrição do Problema', 'Status', 'Prioridade',
                         'Data de Abertura', 'Data de Resolução', 'Responsável',
                         'Categoria do Problema', 'Custo de Reparo', 'Ativo de TI'])  # Cabeçalhos
        for chamada in chamadas:
            writer.writerow([
                chamada.sala_origem,
                chamada.descricao_problema,
                chamada.status,
                chamada.prioridade,
                chamada.data_abertura,
                chamada.data_resolucao,
                chamada.responsavel if chamada.responsavel else '',  # Nome do responsável, se existir
                chamada.categoria_problema,  # Nome da categoria do problema
                chamada.custo_reparo if chamada.custo_reparo is not None else '',  # Custo de reparo, se existir
                chamada.ativo_ti.codigo,  # Código do Ativo de TI
            ])

        return response

@method_decorator(login_required, name='dispatch')
class ExportarAtivoCSVView(View):
    def get(self, request, *args, **kwargs):
        # Obtenha os dados do modelo (ajuste conforme necessário)
        ativos = AtivoTI.objects.all()

        # Configurar a resposta HTTP para um arquivo CSV
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="exportacao_ativos.csv"'

        # Cria um escritor CSV e escreve os cabeçalhos e dados
        writer = csv.writer(response)
        writer.writerow(['Código', 'Nome', 'Foto', 'Tipo', 'Marca', 'Situação',
                         'Número de Série', 'Data de Aquisição', 'Valor Estimado'])  # Cabeçalhos
        for ativo in ativos:
            writer.writerow([
                ativo.codigo,
                ativo.nome,
                ativo.foto,
                ativo.tipo,
                ativo.marca,
                ativo.situacao,
                ativo.numero_serie if ativo.numero_serie is not None else '',  # Número de Série, se existir
                ativo.data_aquisicao if ativo.data_aquisicao is not None else '',  # Data de Aquisição, se existir
                ativo.valor_estimado if ativo.valor_estimado is not None else '',  # Valor Estimado, se existir
            ])

        return response

@method_decorator(login_required, name='dispatch')
class ExportarJSONChamadaView(View):
    def get(self, request, *args, **kwargs):
        # Obtenha os dados do modelo (ajuste conforme necessário)
        chamadas = Chamada.objects.all()

        # Serializa os dados para JSON
        chamadas_json = json.dumps([{
            'sala_origem': chamada.sala_origem,
            'descricao_problema': chamada.descricao_problema,
            'status': chamada.status,
            'prioridade': chamada.prioridade,
            'data_abertura': chamada.data_abertura.strftime('%Y-%m-%d %H:%M:%S'),
            'data_resolucao': chamada.data_resolucao.strftime('%Y-%m-%d %H:%M:%S') if chamada.data_resolucao is not None else None,
            'responsavel': chamada.responsavel.user.username if chamada.responsavel and chamada.responsavel.user else None,
            'categoria_problema': chamada.categoria_problema.nome if chamada.categoria_problema else None,
            'custo_reparo': chamada.custo_reparo,
            'ativo_ti': chamada.ativo_ti.nome if chamada.ativo_ti else None,
        } for chamada in chamadas], indent=2, cls=DjangoJSONEncoder, ensure_ascii=False)

        # Configurar a resposta HTTP para um arquivo JSON
        response = HttpResponse(chamadas_json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="exportacao_chamadas.json"'

        return response

@method_decorator(login_required, name='dispatch')
class ExportarJSONAtivoTIView(View):
    def get(self, request, *args, **kwargs):
        # Obtenha os dados do modelo (ajuste conforme necessário)
        ativos = AtivoTI.objects.all()

        # Serializa os dados para JSON
        ativos_json = json.dumps([{
            'codigo': ativo.codigo,
            'nome': ativo.nome,
            'foto': ativo.foto,
            'tipo': ativo.tipo,
            'marca': ativo.marca,
            'situacao': ativo.situacao,
            'numero_serie': ativo.numero_serie,
            'data_aquisicao': ativo.data_aquisicao.strftime('%Y-%m-%d') if ativo.data_aquisicao is not None else None,
            'valor_estimado': ativo.valor_estimado,
        } for ativo in ativos], indent=2, cls=DjangoJSONEncoder, ensure_ascii=False)

        # Configurar a resposta HTTP para um arquivo JSON
        response = HttpResponse(ativos_json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="exportacao_ativos.json"'

        return response

@method_decorator(login_required, name='dispatch')
class ExportarXMLChamadaView(View):
    def get(self, request, *args, **kwargs):
        # Obtenha os dados do modelo (ajuste conforme necessário)
        chamadas = Chamada.objects.all()

        # Serializa os dados para XML
        chamadas_xml = serialize('xml', chamadas, indent=4)

        # Configurar a resposta HTTP para um arquivo XML
        response = HttpResponse(chamadas_xml, content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename="exportacao_chamadas.xml"'

        return response

@method_decorator(login_required, name='dispatch')
class ExportarXMLAtivoTIView(View):
    def get(self, request, *args, **kwargs):
        # Obtenha os dados do modelo (ajuste conforme necessário)
        ativos = AtivoTI.objects.all()

        # Serializa os dados para XML
        ativos_xml = serialize('xml', ativos, indent=4)

        # Configurar a resposta HTTP para um arquivo XML
        response = HttpResponse(ativos_xml, content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename="exportacao_ativos.xml"'

        return response

@login_required
def exportar_qrcodes_para_pdf(request):
    # Criar um buffer para armazenar o PDF
    buffer = BytesIO()

    # Criar o objeto PDF
    pdf = canvas.Canvas(buffer)

    # Lista de Ativos
    ativos = AtivoTI.objects.all()
    pdf.drawString(100, 750, 'Informações sobre Ativos de TI')

    # Iterar sobre cada Ativo e adicionar QR Code ao PDF
    for ativo in ativos:
        url_ativo = request.build_absolute_uri(reverse('ativo_detail', args=[ativo.id]))

        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=5,
            border=4,
        )
        qr.add_data(url_ativo)
        qr.make(fit=True)

        img = qr.make_image(fill_color="black", back_color="white")

        buffer_img = BytesIO()
        img.save(buffer_img)
        buffer_img.seek(0)

        # Converter a imagem para BytesIO
        image_file = InMemoryUploadedFile(
            buffer_img,
            None,
            f'{ativo.codigo}_qrcode.png',
            'image/png',
            buffer_img.getbuffer().nbytes,
            None
        )

        # Converter BytesIO para Image
        img = Image.open(image_file)

        # Adicionar uma nova página ao PDF
        pdf.showPage()

        # Inserir QR Code no PDF
        pdf.drawInlineImage(img, 100, 500)

        # Adicionar informações do Ativo
        pdf.drawString(100, 430, f'Código do Ativo: {ativo.codigo}')
        pdf.drawString(100, 410, f'Nome do Ativo: {ativo.nome}')
        pdf.drawString(100, 390, f'Tipo de Ativo: {ativo.tipo}')
        pdf.drawString(100, 370, f'Marca: {ativo.marca}')
        pdf.drawString(100, 350, f'Situação: {ativo.situacao}')
        pdf.drawString(100, 330, f'Número de Série: {ativo.numero_serie}')
        pdf.drawString(100, 310, f'Data de Aquisição: {ativo.data_aquisicao}')
        pdf.drawString(100, 290, f'Valor Estimado: R$ {ativo.valor_estimado}')

    # Fechar o objeto PDF
    pdf.save()

    # Retornar o buffer com o conteúdo do PDF
    buffer.seek(0)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="qrcodes.pdf"'
    response.write(buffer.read())

    return response

@login_required
def exportar_qrcode_para_pdf(request, ativo_id):
    # Criar um buffer para armazenar o PDF
    buffer = BytesIO()

    # Criar o objeto PDF
    pdf = canvas.Canvas(buffer)

    # Lista de Ativos
    ativo = get_object_or_404(AtivoTI, id=ativo_id)
    pdf.drawString(100, 750, 'Informações sobre Ativos de TI')

    url_ativo = request.build_absolute_uri(reverse('ativo_detail', args=[ativo.id]))

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=5,
        border=4,
    )
    qr.add_data(url_ativo)
    qr.make(fit=True)

    img = qr.make_image(fill_color="black", back_color="white")

    buffer_img = BytesIO()
    img.save(buffer_img)
    buffer_img.seek(0)

    # Converter a imagem para BytesIO
    image_file = InMemoryUploadedFile(
        buffer_img,
        None,
        f'{ativo.codigo}_qrcode.png',
        'image/png',
        buffer_img.getbuffer().nbytes,
        None
    )

    # Converter BytesIO para Image
    img = Image.open(image_file)

    # Adicionar uma nova página ao PDF
    pdf.showPage()

    # Inserir QR Code no PDF
    pdf.drawInlineImage(img, 100, 500)

    # Adicionar informações do Ativo
    pdf.drawString(100, 430, f'Código do Ativo: {ativo.codigo}')
    pdf.drawString(100, 410, f'Nome do Ativo: {ativo.nome}')
    pdf.drawString(100, 390, f'Tipo de Ativo: {ativo.tipo}')
    pdf.drawString(100, 370, f'Marca: {ativo.marca}')
    pdf.drawString(100, 350, f'Situação: {ativo.situacao}')
    pdf.drawString(100, 330, f'Número de Série: {ativo.numero_serie}')
    pdf.drawString(100, 310, f'Data de Aquisição: {ativo.data_aquisicao}')
    pdf.drawString(100, 290, f'Valor Estimado: R$ {ativo.valor_estimado}')

    # Fechar o objeto PDF
    pdf.save()

    # Retornar o buffer com o conteúdo do PDF
    buffer.seek(0)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = f'attachment; filename="{ativo.codigo}_qrcode.pdf"'
    response.write(buffer.read())

    return response