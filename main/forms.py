from .models import ManutencaoAtivo, Chamada, AtivoTI, Comentario, ChamadaAtivo
from django.utils import timezone
from django import forms

class AtivoTIForm(forms.ModelForm):
    class Meta:
        model = AtivoTI
        fields = ['codigo', 'nome', 'foto', 'tipo', 'marca', 'especificacoes', 'situacao', 'numero_serie', 'data_aquisicao', 'valor_estimado']

    def clean_data_aquisicao(self):
        data_aquisicao = self.cleaned_data.get('data_aquisicao')

        if data_aquisicao and data_aquisicao > timezone.now().date():
            raise forms.ValidationError("A data de aquisição não pode estar no futuro.")

        return data_aquisicao

    def clean_valor_estimado(self):
        valor_estimado = self.cleaned_data.get('valor_estimado')

        if valor_estimado and valor_estimado <= 0:
            raise forms.ValidationError("O valor estimado deve ser um número decimal positivo.")

        return valor_estimado

class ChamadaForm(forms.ModelForm):
    class Meta:
        model = Chamada
        fields = ['sala_origem', 'descricao_problema', 'status', 'prioridade', 'responsavel', 'categoria_problema', 'custo_reparo']

    def clean_custo_reparo(self):
        custo_reparo = self.cleaned_data.get('custo_reparo')

        if custo_reparo and custo_reparo <= 0:
            raise forms.ValidationError("O custo de reparo deve ser um número decimal positivo.")

        return custo_reparo

class ChamadaForm2(forms.ModelForm):
    class Meta:
        model = Chamada
        fields = ['ativo_ti', 'sala_origem', 'descricao_problema', 'status', 'prioridade', 'responsavel', 'categoria_problema', 'custo_reparo']

    def clean_custo_reparo(self):
        custo_reparo = self.cleaned_data.get('custo_reparo')

        if custo_reparo and custo_reparo <= 0:
            raise forms.ValidationError("O custo de reparo deve ser um número decimal positivo.")

        return custo_reparo

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['texto']

class ManutencaoForm(forms.ModelForm):
    class Meta:
        model = ManutencaoAtivo
        fields = ['descricao', 'data_inicio', 'data_conclusao', 'custo', 'responsavel']

    def clean_data_inicio(self):
        data_inicio = self.cleaned_data.get('data_inicio')

        if data_inicio and data_inicio > timezone.now().date():
            raise forms.ValidationError("A data de início não pode estar no futuro.")

        return data_inicio

    def clean_data_conclusao(self):
        data_conclusao = self.cleaned_data.get('data_conclusao')
        data_inicio = self.cleaned_data.get('data_inicio')

        if data_inicio and data_conclusao:
            if data_conclusao < data_inicio:
                raise forms.ValidationError("A data de conclusão não pode ser anterior à data de início.")
        
            if data_conclusao > timezone.now().date():
                raise forms.ValidationError("A data de conclusão não pode estar no futuro.")

        return data_conclusao

    def clean_custo(self):
        custo = self.cleaned_data.get('custo')

        if custo and custo <= 0:
            raise forms.ValidationError("O custo deve ser um número decimal positivo.")

        return custo

class ManutencaoAtivoForm(forms.ModelForm):
    ativos = forms.ModelMultipleChoiceField(queryset=AtivoTI.objects.all(), widget=forms.SelectMultiple)

    class Meta:
        model = ManutencaoAtivo
        fields = ['ativos', 'descricao', 'data_inicio', 'data_conclusao', 'custo', 'responsavel']

    def clean_data_inicio(self):
        data_inicio = self.cleaned_data.get('data_inicio')

        if data_inicio and data_inicio > timezone.now().date():
            raise forms.ValidationError("A data de início não pode estar no futuro.")

        return data_inicio

    def clean_data_conclusao(self):
        data_conclusao = self.cleaned_data.get('data_conclusao')
        data_inicio = self.cleaned_data.get('data_inicio')

        if data_inicio and data_conclusao:
            if data_conclusao < data_inicio:
                raise forms.ValidationError("A data de conclusão não pode ser anterior à data de início.")
        
            if data_conclusao > timezone.now().date():
                raise forms.ValidationError("A data de conclusão não pode estar no futuro.")

        return data_conclusao

    def clean_custo(self):
        custo = self.cleaned_data.get('custo')

        if custo and custo <= 0:
            raise forms.ValidationError("O custo deve ser um número decimal positivo.")

        return custo

class ChamadaAtivoForm(forms.ModelForm):
    ativo = forms.ModelMultipleChoiceField(queryset=AtivoTI.objects.all(), widget=forms.SelectMultiple)

    class Meta:
        model = ChamadaAtivo
        fields = ['chamada', 'ativo']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['ativo'].widget.attrs['multiple'] = True
        self.fields['chamada'].queryset = Chamada.objects.all()

    def clean_ativo(self):
        cleaned_data = super().clean()
        chamada = cleaned_data.get('chamada')
        ativos_selecionados = cleaned_data.get('ativo')
        
        if chamada and ativos_selecionados:
            for ativo in ativos_selecionados:
                if ChamadaAtivo.objects.filter(chamada=chamada, ativos=ativo).exists():
                    raise forms.ValidationError(f"O ativo {ativo} já está associado a este chamado.")
        return ativos_selecionados

class ChamadasAtivoForm(forms.ModelForm):
    chamadas = forms.ModelMultipleChoiceField(queryset=Chamada.objects.all(), widget=forms.SelectMultiple)
    ativos = forms.ModelChoiceField(queryset=AtivoTI.objects.all(), widget=forms.Select)

    class Meta:
        model = ChamadaAtivo
        fields = ['ativos', 'chamadas']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['chamadas'].widget.attrs['multiple'] = True
        self.fields['ativos'].queryset = AtivoTI.objects.all()

    def clean_ativo(self):
        cleaned_data = super().clean()
        ativos_selecionados = cleaned_data.get('ativos')
        chamada = cleaned_data.get('chamadas')

        if chamada and ativos_selecionados:
            for ativo in ativos_selecionados:
                if ChamadaAtivo.objects.filter(chamada=chamada, ativos=ativo).exists():
                    raise forms.ValidationError(f"O ativo {ativo} já está associado a este chamado.")
        return ativos_selecionados

class ChamadaAtivoFormSemChamada(forms.ModelForm):
    ativo = forms.ModelMultipleChoiceField(queryset=AtivoTI.objects.all(), widget=forms.SelectMultiple)

    class Meta:
        model = ChamadaAtivo
        fields = ['ativo']

    def __init__(self, chamada_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chamada_id = chamada_id

    def clean_ativo(self):
        cleaned_data = super().clean()
        ativos_selecionados = cleaned_data.get('ativo')

        if ativos_selecionados:
            for ativo in ativos_selecionados:
                if ChamadaAtivo.objects.filter(chamada=self.chamada_id, ativos=ativo).exists():
                    raise forms.ValidationError(f"O ativo {ativo} já está associado a este chamado.")
        return ativos_selecionados

class ChamadaAtivoFormChamada(forms.ModelForm):
    chamadas = forms.ModelMultipleChoiceField(queryset=Chamada.objects.all(), widget=forms.SelectMultiple)

    class Meta:
        model = ChamadaAtivo
        fields = ['chamadas']

    def __init__(self, ativo_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ativo_id = ativo_id

    def clean(self):
        cleaned_data = super().clean()
        ativo = AtivoTI.objects.get(id=self.ativo_id)
        chamadas_selecionadas = cleaned_data.get('chamadas')

        if chamadas_selecionadas:
            for chamada in chamadas_selecionadas:
                if ChamadaAtivo.objects.filter(chamada=chamada, ativos=ativo).exists():
                    raise forms.ValidationError(f"O ativo já está associado à chamada {chamada}.")

        return cleaned_data