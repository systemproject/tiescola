from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from .models import Chamada, AtivoTI, CategoriaProblema
from django.contrib.auth.models import User

class MainViewsTestCase(TestCase):
    def setUp(self):
        # Crie um usuário para simular o login
        self.user = User.objects.create_user(username='testuser', password='testpassword')

        # Criar objetos de AtivoTI para os testes
        self.ativo = AtivoTI.objects.create(codigo='A001', nome='Ativo1', tipo='Computador', marca='Dell', situacao='Em Uso')

        self.categoria_problema = CategoriaProblema.objects.create(nome='Hardware')

        # Criar objetos de Chamada associando a um AtivoTI
        self.chamada = Chamada.objects.create(
            sala_origem='Sala1',
            descricao_problema='Problema1',
            status='Aberto',
            prioridade='Alta',
            ativo_ti=self.ativo,
            categoria_problema_id=self.categoria_problema.id
        )

    def test_index_view(self):
        # Testar a view index
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/index.html')

    def test_data_export_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('exportar_dados'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/exportar_dados.html')

    def test_status_chart_view(self):
        # Testar a view status_chart
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('status'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/chart_status.html')

    def test_prioridade_chart_view(self):
        # Testar a view prioridade_chart
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('prioridade'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/chart_prioridade.html')

    def test_listar_chamadas_view(self):
        # Testar a view listar_chamadas
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('listar_chamadas'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/listar_chamadas.html')

    def test_listar_ativos_view(self):
        # Testar a view listar_ativos
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('listar_ativos'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/listar_ativos.html')

    def test_ativo_detail_view(self):
        # Testar a view ativo_detail_view
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('ativo_detail', args=[self.ativo.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/detalhes_ativo.html')
        self.assertEqual(response.context['ativo'], self.ativo)

    def test_chamada_detail_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('chamada_detail', args=[self.chamada.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/detalhes_chamada.html')
        self.assertEqual(response.context['chamada'], self.chamada)

    def test_exportar_chamada_csv_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('exportar_chamadas_csv'))
        self.assertEqual(response.status_code, 200)

    def test_exportar_ativo_csv_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('exportar_ativos_csv'))
        self.assertEqual(response.status_code, 200)

    def test_exportar_json_chamada_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('exportar_chamadas_json'))
        self.assertEqual(response.status_code, 200)

    def test_exportar_json_ativo_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('exportar_ativos_json'))
        self.assertEqual(response.status_code, 200)

    def test_exportar_xml_chamada_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('exportar_chamadas_xml'))
        self.assertEqual(response.status_code, 200)

    def test_exportar_xml_ativo_view(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('exportar_ativos_xml'))
        self.assertEqual(response.status_code, 200)

    def test_index_view_authenticated(self):
        # Testar a view index para usuário autenticado
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/index.html')

    def test_index_view_unauthenticated(self):
        # Testar a view index para usuário não autenticado
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login
        self.assertRedirects(response, '/login/?next=%2F')

    def test_exportar_xml_ativo_view_unauthenticated(self):
        # Testar a view de exportação de XML para ativos para usuário não autenticado
        response = self.client.get(reverse('exportar_ativos_xml'))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def test_exportar_csv_chamada_view_unauthenticated(self):
        # Testar a view de exportação de CSV para chamadas para usuário não autenticado
        response = self.client.get(reverse('exportar_chamadas_csv'))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def test_listar_chamadas_view_filtered(self):
        # Testar a view listar_chamadas com filtros aplicados
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('listar_chamadas'), {'salaFilter': 'Sala1', 'statusFilter': 'Aberto'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/listar_chamadas.html')

    def test_listar_ativos_view_filtered(self):
        # Testar a view listar_ativos com filtros aplicados
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('listar_ativos'), {'tipoFilter': 'Computador', 'marcaFilter': 'Dell'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/listar_ativos.html')

    def test_ativo_detail_view_unauthenticated(self):
        # Testar a view ativo_detail_view para usuário não autenticado
        response = self.client.get(reverse('ativo_detail', args=[self.ativo.id]))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def test_chamada_detail_view_unauthenticated(self):
        # Testar a view chamada_detail_view para usuário não autenticado
        response = self.client.get(reverse('chamada_detail', args=[self.chamada.id]))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def test_status_chart_view_unauthenticated(self):
        # Testar a view status_chart para usuário não autenticado
        response = self.client.get(reverse('status'))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def test_prioridade_chart_view_unauthenticated(self):
        # Testar a view prioridade_chart para usuário não autenticado
        response = self.client.get(reverse('prioridade'))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def test_listar_chamadas_view_unauthenticated(self):
        # Testar a view listar_chamadas para usuário não autenticado
        response = self.client.get(reverse('listar_chamadas'))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def test_listar_ativos_view_unauthenticated(self):
        # Testar a view listar_ativos para usuário não autenticado
        response = self.client.get(reverse('listar_ativos'))
        self.assertEqual(response.status_code, 302)  # Redireciona para a página de login

    def tearDown(self):
        # Limpar dados após os testes, se necessário
        User.objects.all().delete()
        Chamada.objects.all().delete()
        AtivoTI.objects.all().delete()