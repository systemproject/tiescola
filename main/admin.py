from .models import CategoriaProblema, AtivoTI, Responsavel, Chamada, Comentario, ManutencaoAtivo
from import_export.admin import ImportExportModelAdmin, ExportMixin
from import_export.formats import base_formats
from django.contrib import admin

admin.site.register(CategoriaProblema)
admin.site.register(Responsavel)

class AtivoTIAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('nome', 'id', 'tipo', 'marca', 'numero_serie', 'data_aquisicao')
    list_filter = ('tipo', 'marca', 'data_aquisicao')
    search_fields = ('codigo','nome', 'numero_serie')

    def get_export_formats(self):
            """
            Returns available export formats.
            """
            formats = (
                  base_formats.CSV,
                  base_formats.XLS,
                  base_formats.XLSX,
                  base_formats.TSV,
                  base_formats.ODS,
                  base_formats.JSON,
                  base_formats.HTML,
            )
            return [f for f in formats if f().can_export()]

admin.site.register(AtivoTI, AtivoTIAdmin)

class ChamadaAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'sala_origem', 'status', 'data_abertura', 'responsavel')
    list_filter = ('status', 'data_abertura')
    search_fields = ('id', 'sala_origem', 'responsavel__user__username')

    def get_export_formats(self):
            """
            Returns available export formats.
            """
            formats = (
                  base_formats.CSV,
                  base_formats.XLS,
                  base_formats.XLSX,
                  base_formats.TSV,
                  base_formats.ODS,
                  base_formats.JSON,
                  base_formats.HTML,
            )
            return [f for f in formats if f().can_export()]

admin.site.register(Chamada, ChamadaAdmin)

class ComentarioAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('chamada', 'autor', 'data_publicacao')
    list_filter = ('autor', 'data_publicacao')
    search_fields = ('autor',)

admin.site.register(Comentario, ComentarioAdmin)

class ManutencaoAtivoAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('ativo', 'data_inicio', 'data_conclusao', 'custo', 'responsavel')
    list_filter = ('ativo', 'data_conclusao')
    search_fields = ('ativo',)

    def get_export_formats(self):
            """
            Returns available export formats.
            """
            formats = (
                  base_formats.CSV,
                  base_formats.XLS,
                  base_formats.XLSX,
                  base_formats.TSV,
                  base_formats.ODS,
                  base_formats.JSON,
                  base_formats.HTML,
            )
            return [f for f in formats if f().can_export()]

admin.site.register(ManutencaoAtivo, ManutencaoAtivoAdmin)