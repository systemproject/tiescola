from django.db import models
from django.contrib.auth.models import User

class CategoriaProblema(models.Model):
    nome = models.CharField(max_length=50)

    def __str__(self):
        return self.nome

class AtivoTI(models.Model):
    codigo = models.CharField(max_length=50, unique=True)
    nome = models.CharField(max_length=50)
    foto = models.URLField(max_length=200, 
    	default='https://informaticacebrac.netlify.app/images/computer.png', 
    	null=True, blank=True)
    
    TIPOS_DE_ATIVOS = [
        ('Computador', 'Computador'),
        ('Periférico', 'Periférico'),
        ('Impressora', 'Impressora'),
        ('Eletrônico', 'Eletrônico'),
        ('Servidor', 'Servidor'),
        ('Roteador', 'Roteador'),
        ('Switch', 'Switch'),
        ('Outros', 'Outros'),
    ]
    tipo = models.CharField(max_length=25, choices=TIPOS_DE_ATIVOS, default='Computador')

    MARCAS = [
        ('Dell', 'Dell'),
        ('HP', 'HP'),
        ('Lenovo', 'Lenovo'),
        ('Cisco', 'Cisco'),
        ('Philco', 'Philco'),
        ('Philips', 'Philips'),
        ('Outra', 'Outra'),
    ]
    marca = models.CharField(max_length=25, choices=MARCAS, default='Outra')

    especificacoes = models.TextField(blank=True, null=True)

    SITUACOES = [
        ('Funcionando', 'Funcionando'),
        ('Danificado', 'Danificado'),
        ('Inutilizável', 'Inutilizável'),
    ]
    situacao = models.CharField(max_length=25, choices=SITUACOES, default='Funcionando')

    # Adicione outras propriedades relevantes, como número de série, data de aquisição, etc.
    numero_serie = models.CharField(max_length=50, blank=True, null=True)
    data_aquisicao = models.DateField(blank=True, null=True)
    valor_estimado = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    def get_situacao_data(self):
        situacoes = dict(self.SITUACOES)
        data = []

        for situacao_key, situacao_label in situacoes.items():
            count = AtivoTI.objects.filter(situacao=situacao_key).count()
            data.append({'label': situacao_label, 'count': count})

        return data

    def get_tipo_data(self):
        tipos = dict(self.TIPOS_DE_ATIVOS)
        data = []

        for tipo_key, tipo_label in tipos.items():
            count = AtivoTI.objects.filter(tipo=tipo_key).count()
            data.append({'label': tipo_label, 'count': count})

        return data

    def get_marcas_data(self):
        marcas = dict(self.MARCAS)
        data = []

        for marca_key, marca_label in marcas.items():
            count = AtivoTI.objects.filter(marca=marca_key).count()
            data.append({'label': marca_label, 'count': count})

        return data

    def get_numero_chamadas(self):
        return self.chamada_set.count()

    def get_numero_manutencoes(self):
        return self.manutencaoativo_set.count()
    
    def __str__(self):
        return f'{self.nome}'

class Responsavel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cargo = models.CharField(max_length=50)

    def __str__(self):
        return self.user.username

class Chamada(models.Model):
    STATUS_CHOICES = [
        ('Aberta', 'Aberta'),
        ('Atribuida', 'Atribuida'),
        ('Resolvida', 'Resolvida'),
    ]

    PRIORIDADE_CHOICES = [
        ('Baixa', 'Baixa'),
        ('Média', 'Média'),
        ('Alta', 'Alta'),
    ]

    SALAS = [
        ('Marketing', 'Marketing'),
        ('Recepção', 'Recepção'),
        ('Sala 01', 'Sala 01'),
        ('Sala Pequena', 'Sala Pequena'),
        ('Sala Inglês', 'Sala Inglês'),
        ('Sala Saúde', 'Sala Saúde'),
        ('Laboratório Informática', 'Laboratório Informática'),
        ('Coordenadoria', 'Coordenadoria'),
        ('Comercial', 'Comercial'),
        ('CRA', 'CRA'),
    ]

    sala_origem = models.CharField(max_length=50, choices=SALAS, default='Laboratório Informática')
    descricao_problema = models.TextField()
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='Aberta')
    prioridade = models.CharField(max_length=10, choices=PRIORIDADE_CHOICES, default='Média')
    data_abertura = models.DateTimeField(auto_now_add=True)
    data_resolucao = models.DateTimeField(null=True, blank=True)
    responsavel = models.ForeignKey(Responsavel, on_delete=models.SET_NULL, null=True, blank=True)
    categoria_problema = models.ForeignKey(CategoriaProblema, on_delete=models.CASCADE)
    custo_reparo = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    ativo_ti = models.ForeignKey(AtivoTI, on_delete=models.CASCADE)

    def get_prioridade_data(self):
        prioridades = dict(self.PRIORIDADE_CHOICES)
        data = []

        for prioridade_key, prioridade_label in prioridades.items():
            count = Chamada.objects.filter(prioridade=prioridade_key).count()
            data.append({'label': prioridade_label, 'count': count})

        return data

    def get_status_data(self):
        status = dict(self.STATUS_CHOICES)
        data = []

        for status_key, status_label in status.items():
            count = Chamada.objects.filter(status=status_key).count()
            data.append({'label': status_label, 'count': count})

        return data

    def get_sala_origem_data(self):
        salas = dict(self.SALAS)
        data = []

        for salas_key, salas_label in salas.items():
            count = Chamada.objects.filter(sala_origem=salas_key).count()
            data.append({'label': salas_label, 'count': count})

        return data

    def __str__(self):
        return f'Chamada {self.id} - {self.status}'

class Comentario(models.Model):
    chamada = models.ForeignKey('Chamada', on_delete=models.CASCADE)
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    texto = models.TextField()
    data_publicacao = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Comentário por {self.autor.username} em {self.data_publicacao}'

class ManutencaoAtivo(models.Model):
    ativo = models.ForeignKey(AtivoTI, on_delete=models.CASCADE)
    descricao = models.TextField()
    data_inicio = models.DateField()
    data_conclusao = models.DateField(null=True, blank=True)
    custo = models.DecimalField(max_digits=10, decimal_places=2)
    responsavel = models.ForeignKey(Responsavel, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return f"Manutenção de {self.ativo.nome} - {self.data_inicio}"

class ChamadaAtivo(models.Model):
    chamada = models.ForeignKey(Chamada, on_delete=models.CASCADE)
    ativos = models.ManyToManyField(AtivoTI)

    def __str__(self):
        return f"{self.chamada} - {', '.join(str(ativo) for ativo in self.ativos.all())}"